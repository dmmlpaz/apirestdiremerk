package com.apidirec.src.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-01-13T16:18:34")
@StaticMetamodel(PuenteSec.class)
public class PuenteSec_ { 

    public static volatile SingularAttribute<PuenteSec, Date> fecha_pedido;
    public static volatile SingularAttribute<PuenteSec, Character> estado;
    public static volatile SingularAttribute<PuenteSec, Date> fecha_retoma;
    public static volatile SingularAttribute<PuenteSec, String> ciudad;
    public static volatile SingularAttribute<PuenteSec, String> direccion;
    public static volatile SingularAttribute<PuenteSec, Date> fecha_sector;
    public static volatile SingularAttribute<PuenteSec, Integer> id_pedido;
    public static volatile SingularAttribute<PuenteSec, String> sector;

}