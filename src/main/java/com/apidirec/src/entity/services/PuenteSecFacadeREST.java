/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.entity.services;

import com.apidirec.src.entity.PuenteSec;
import com.apidirec.src.exception.ExceptionControlada;
import com.apidirec.src.util.Constant;
import com.apidirec.src.util.Utilidades;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.eclipse.persistence.config.QueryHints;

/**
 *
 * @author Usuario
 */
@Stateless
@Path("puentesec")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class PuenteSecFacadeREST extends AbstractFacade<PuenteSec> {

    static org.apache.logging.log4j.Logger log = LogManager.getLogger(PuenteSecFacadeREST.class.getName());

    @PersistenceContext(unitName = "apidirecmerklist_WSRestDirecMerkList_war_1.0PU")
    private EntityManager em;

    public PuenteSecFacadeREST() {
        super(PuenteSec.class);
    }

    @POST
    public Response createPuenteSec(PuenteSec puenteSecIN) {
        log.info("init createPuenteSec");
        PuenteSec puenteSec = null;
        String query = null;
        List<PuenteSec> listPuenteSec = new ArrayList<>();
        JsonObject jsonReturn = Json.createObjectBuilder().add("sector", "").build();
        if (Optional.ofNullable(puenteSecIN).isPresent()) {
            if ((puenteSecIN.getId_pedido() == null ? "" : puenteSecIN.getId_pedido().toString()).isEmpty()) {
                throw new ExceptionControlada(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode(), Response.Status.NOT_FOUND.toString(), Constant.PARAMETER_ID_PEDIDO_NULL);
            }
            if ((puenteSecIN.getDireccion() == null ? "" : puenteSecIN.getDireccion()).isEmpty()) {
                throw new ExceptionControlada(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode(), Response.Status.NOT_FOUND.toString(), Constant.PARAMETER_ID_PEDIDO_NULL);
            }
            query = "SELECT * FROM PUENTE_SEC WHERE id_pedido = " + puenteSecIN.getId_pedido() + " AND direccion = '" + puenteSecIN.getDireccion() + "' ";
            log.info("query:" + query);
            listPuenteSec = getEntityManager().createNativeQuery(query, PuenteSec.class).setHint(QueryHints.REFRESH, true).getResultList();
            if (listPuenteSec.isEmpty()) {
                //se valida si existe el id de pedido en base de datos
                query = "SELECT * FROM PUENTE_SEC WHERE id_pedido = " + puenteSecIN.getId_pedido() + " ";
                listPuenteSec = getEntityManager().createNativeQuery(query, PuenteSec.class).setHint(QueryHints.REFRESH, true).getResultList();
                if (listPuenteSec.isEmpty()) {
                    try {
                        puenteSec = new PuenteSec();
                        puenteSec.setId_pedido(puenteSecIN.getId_pedido());
                        puenteSec.setDireccion(puenteSecIN.getDireccion());
                        puenteSec.setCiudad(Constant.CITY_ORIGIN);
                        puenteSec.setFecha_pedido(Utilidades.sysdateHour());
                        puenteSec.setEstado(Constant.STATE_1);
                        puenteSec.setFecha_retoma(Utilidades.sysdateHour());
                        super.create(puenteSec);
                        Thread.sleep(6000);
                        query = "SELECT * FROM PUENTE_SEC WHERE id_pedido = " + puenteSec.getId_pedido() + " AND direccion = '" + puenteSec.getDireccion() + "' ";
                        listPuenteSec = getEntityManager().createNativeQuery(query, PuenteSec.class).setHint(QueryHints.REFRESH, true).getResultList();
                        if (!listPuenteSec.isEmpty()) {
                            puenteSec = listPuenteSec.get(0);
                            if (!(puenteSec.getSector() == null ? "" : puenteSec.getSector()).isEmpty()) {
                                jsonReturn = Json.createObjectBuilder().add("sector", puenteSec.getSector()).build();
                            }
                        }
                    } catch (InterruptedException ex) {
                        throw new ExceptionControlada(Response.Status.INTERNAL_SERVER_ERROR, Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), Response.Status.INTERNAL_SERVER_ERROR.toString(), Constant.ERROR_INSERT_SLEEP.replace("@1", String.valueOf(puenteSecIN.getId_pedido())));
                    }
                } else {
                    throw new ExceptionControlada(Response.Status.INTERNAL_SERVER_ERROR, Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), Response.Status.INTERNAL_SERVER_ERROR.toString(), Constant.ID_PEDIDO_EXISTS.replace("@1", String.valueOf(puenteSecIN.getId_pedido())));
                }
            } else {
                log.info("exists address return listPuenteSec all");
                puenteSec = listPuenteSec.get(0);
                if (!(puenteSec.getSector() == null ? "" : puenteSec.getSector()).isEmpty()) {
                    jsonReturn = Json.createObjectBuilder().add("sector", puenteSec.getSector()).build();
                }
            }
        }
        return Response.ok(jsonReturn).build();
    }

    @GET
    @Path("{id}")
    public Response findPuenteSec(@PathParam("id") Integer id) {
        log.info("init findPuenteSec");
        return Response.ok(super.find(id)).build();
    }

    @GET
    public Response findAllPuenteSec() {
        log.info("init findAllPuenteSec");
        return Response.ok(super.findAll()).build();
    }

    @GET
    @Path("count")
    public String countPuenteSec() {
        log.info("init countREST");
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
