/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.entity.services;

import java.util.Set;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.HEADER_DECORATOR)
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.apidirec.src.entity.services.ApplicationConfig.class);
        resources.add(com.apidirec.src.entity.services.CorsFilterRequest.class);
        resources.add(com.apidirec.src.entity.services.CorsFilterResponse.class);
        resources.add(com.apidirec.src.entity.services.PuenteSecFacadeREST.class);
        resources.add(com.apidirec.src.exception.ConstraintViolationExceptionMapper.class);
        resources.add(com.apidirec.src.exception.ExceptionControladaMapper.class);
        resources.add(com.apidirec.src.exception.ExceptionGeneralMapper.class);
        resources.add(com.apidirec.src.exception.RollbackExceptionMapper.class);
        resources.add(com.apidirec.src.exception.SQLExceptionMapper.class);
        resources.add(com.apidirec.src.exception.ValidationExceptionMapper.class);
    }

}
