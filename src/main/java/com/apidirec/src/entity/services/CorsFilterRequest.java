/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.entity.services;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Usuario
 */
@Provider
public class CorsFilterRequest implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        System.out.print("Se corre CorsFilter3 ContainerRequestContext -> filter");
        crc.getHeaders().add("Access-Control-Allow-Origin", "*");
        crc.getHeaders().add("Access-Control-Allow-Credentials", "false");
        crc.getHeaders().add("Access-Control-Allow-Headers", "authorization,origin, content-type, accept, token");
        crc.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        System.out.print("CorsFilterRequest -> filter ContainerRequestFilter");
    }

}
