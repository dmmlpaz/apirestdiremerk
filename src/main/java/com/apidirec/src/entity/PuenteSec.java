/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "puente_sec")
@XmlRootElement
public class PuenteSec implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_pedido")
    private Integer id_pedido;
    @Size(max = 15)
    @Column(name = "ciudad")
    private String ciudad;
    @Size(max = 65)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "fecha_pedido")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_pedido;
    @Column(name = "estado")
    private Character estado;
    @Size(max = 10)
    @Column(name = "sector")
    private String sector;
    @Column(name = "fecha_sector")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_sector;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_retoma")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_retoma;

    public PuenteSec() {
    }

    public Integer getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Integer id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFecha_pedido() {
        return fecha_pedido;
    }

    public void setFecha_pedido(Date fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Date getFecha_sector() {
        return fecha_sector;
    }

    public void setFecha_sector(Date fecha_sector) {
        this.fecha_sector = fecha_sector;
    }

    public Date getFecha_retoma() {
        return fecha_retoma;
    }

    public void setFecha_retoma(Date fecha_retoma) {
        this.fecha_retoma = fecha_retoma;
    }

}
