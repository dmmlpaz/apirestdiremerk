/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;

@Provider
public class ExceptionControladaMapper implements ExceptionMapper<ExceptionControlada> {

    static org.apache.logging.log4j.Logger log = LogManager.getLogger(ExceptionControladaMapper.class.getName());

    @Override
    public Response toResponse(ExceptionControlada exceptionControlada) {
        log.info("toResponse excepcionControlada");
        return Response.status(exceptionControlada.getMensajeError().getEstadoHttp()).entity(exceptionControlada.getMensajeError()).build();
    }

}
