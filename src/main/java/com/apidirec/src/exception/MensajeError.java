/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@XmlRootElement
public class MensajeError {

    private int codigoEstadoHttp;
    private String descripcionEstadoHttp;
    private String descripcionError;
    private Status estadoHttp;

    public MensajeError() {
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    public int getCodigoEstadoHttp() {
        return codigoEstadoHttp;
    }

    public void setCodigoEstadoHttp(int codigoEstadoHttp) {
        this.codigoEstadoHttp = codigoEstadoHttp;
    }

    public String getDescripcionEstadoHttp() {
        return descripcionEstadoHttp;
    }

    public void setDescripcionEstadoHttp(String descripcionEstadoHttp) {
        this.descripcionEstadoHttp = descripcionEstadoHttp;
    }

    public Status getEstadoHttp() {
        return estadoHttp;
    }

    public void setEstadoHttp(Status estadoHttp) {
        this.estadoHttp = estadoHttp;
    }

}
