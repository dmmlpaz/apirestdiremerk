/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Usuario
 */
@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

    private MensajeError mensajeError;
    static org.apache.logging.log4j.Logger log = LogManager.getLogger(ValidationExceptionMapper.class.getName());

    @Override
    public Response toResponse(ValidationException e) {
        mensajeError = new MensajeError();
        return Response.status(Response.Status.BAD_REQUEST).entity(mensajeError).type("application/json").build();
    }

}
