/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Usuario
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private MensajeError mensajeError;
    static org.apache.logging.log4j.Logger log = LogManager.getLogger(ConstraintViolationExceptionMapper.class.getName());

    @Override
    public Response toResponse(ConstraintViolationException e) {
        mensajeError = new MensajeError();
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mensajeError).type("application/json").build();
    }

    private String prepareMessage(ConstraintViolationException exception) {
        String msg = "";
        msg = exception.getConstraintViolations().stream().map((cv) -> cv.getPropertyPath() + " " + cv.getMessage() + "\n").reduce(msg, String::concat);
        return msg;
    }
}
