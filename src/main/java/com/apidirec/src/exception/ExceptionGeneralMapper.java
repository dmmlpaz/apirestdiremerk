/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import com.apidirec.src.util.Constant;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Usuario
 */
@Provider
public class ExceptionGeneralMapper implements ExceptionMapper<Exception> {

    private MensajeError mensajeError;
    static org.apache.logging.log4j.Logger log = LogManager.getLogger(ExceptionMapper.class.getName());

    @Override
    public Response toResponse(Exception e) {
        mensajeError = new MensajeError();
        //mensajeError.setCodigoEstadoHttp(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        mensajeError.setDescripcionEstadoHttp(e.toString());
        mensajeError.setDescripcionError("ExceptionGeneralMapper:" + Constant.ERROR_NOT_CONTROLLED);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).type("application/json").entity(mensajeError).build();
    }
}
