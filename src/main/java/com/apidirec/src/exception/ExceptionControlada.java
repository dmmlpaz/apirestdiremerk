/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.exception;

import javax.ws.rs.core.Response.Status;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Usuario
 */
public class ExceptionControlada extends RuntimeException {

    private MensajeError mensajeError;
    static org.apache.logging.log4j.Logger log = LogManager.getLogger(ExceptionControlada.class.getName());

    public ExceptionControlada(Status estadoHttp, int codigoEstadoHttp, String descripcionEstadoHttp, String descripcionError) {
        super();
        log.info("ExcepcionControlada se corre el constructor");
        mensajeError = new MensajeError();
        mensajeError.setEstadoHttp(estadoHttp);
        mensajeError.setCodigoEstadoHttp(codigoEstadoHttp);
        mensajeError.setDescripcionEstadoHttp(descripcionEstadoHttp);
        mensajeError.setDescripcionError(descripcionError);
    }

    public MensajeError getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(MensajeError mensajeError) {
        this.mensajeError = mensajeError;
    }

}
