/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.util;

/**
 *
 * @author Usuario
 */
public class Constant {

    public static final String CITY_ORIGIN = "BOGOTA";
    public static final char STATE_1 = '1';
    public static final String PARAMETER_ID_PEDIDO_NULL = "El argumento id_pedido es incorrecto";
    public static final String ERROR_NOT_CONTROLLED = "Error no controlado";
    public static final String ID_PEDIDO_EXISTS = "El id de pedido @1 ya esta registrado en la base de datos";
    public static final String ERROR_INSERT_SLEEP = "Error en el temporizador al realizar la inserción del id de pedido @1";
}
