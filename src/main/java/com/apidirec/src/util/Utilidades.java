/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apidirec.src.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class Utilidades {

    public static String getProperties(String key) {
        ResourceBundle rb = ResourceBundle.getBundle("properties.properties_es");
        return rb.getString(key);
    }

    public static String getSysdate(String formato) {
        DateFormat dateFormat = new SimpleDateFormat(formato);
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        return dateFormat.format(date);

    }

    public static String getStringDate(String formato, String fecha) {
        String fechaRetorno = "";
        if (formato != null && fecha != null) {
            if (!formato.isEmpty() && !fecha.isEmpty()) {
                Date date = null;
                DateFormat formatter = new SimpleDateFormat(formato);
                try {
                    date = formatter.parse(fecha);
                } catch (ParseException ex) {
                    Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
                }
                DateFormat dateFormat = new SimpleDateFormat(formato);
                System.out.println(date);
                fechaRetorno = dateFormat.format(date);
            }
        }
        return fechaRetorno;
    }

    public static String getSysdate(String formato, Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat(formato);
        System.out.println(dateFormat.format(fecha));
        return dateFormat.format(fecha);

    }

    public static Date getFechaDesdeCadena(String fecha, String formato) {
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat(formato);
            date = formatter.parse(fecha);
            System.out.println(date);

            SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
            Date dateFecha = null;
            try {
                dateFecha = formatoDelTexto.parse(fecha);
            } catch (ParseException ex) {
            }

        } catch (ParseException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public static Date getDate(String formato, Date inFecha) {
        java.sql.Date sqlDate = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
            Date todayWithZeroTime = dateFormat.parse(dateFormat.format(inFecha));
            sqlDate = new java.sql.Date(todayWithZeroTime.getTime());
        } catch (ParseException ex) {

        }
        return sqlDate;
    }

    public static Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }

    public static Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDate(Date fecha) {
        java.sql.Date sqlDate = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date todayWithZeroTime = formatter.parse(formatter.format(fecha));
            sqlDate = new java.sql.Date(todayWithZeroTime.getTime());
            java.util.Date utilDate = fecha;
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (ParseException ex) {

        }
        return sqlDate;
    }

    public static String getFechaString(String formato, Object fecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
        LocalDate localDate1 = LocalDate.parse((CharSequence) fecha, DateTimeFormatter.ofPattern(formato));
        String stringFecha = localDate1.format(formatter);
        return stringFecha;
    }

    public static Date getFechaObjectToDate(String formato, Object fecha) {
        //LocalDate solo se puede imprimir en formato ISO8601 (aaaa-MM-dd). 
        java.sql.Date date;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato, Locale.getDefault());
        LocalDate localDate = LocalDate.parse((CharSequence) fecha, DateTimeFormatter.ofPattern(formato));
        date = java.sql.Date.valueOf(localDate);
        return date;
    }

    public static Date sysdateHour() {
        Date date = null;
        SimpleDateFormat hourdateFormat = null;
        try {
            date = new Date();
            hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println("hora nueva: " + hourdateFormat.format(date));
            date = hourdateFormat.parse(hourdateFormat.format(date));
        } catch (ParseException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public static Date sysdateHour(String format) {
        Date date = null;
        SimpleDateFormat hourdateFormat = null;
        try {
            date = new Date();
            hourdateFormat = new SimpleDateFormat(format);
            System.out.println("hora nueva: " + hourdateFormat.format(date));
            date = hourdateFormat.parse(hourdateFormat.format(date));
        } catch (ParseException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public static void main(String arg[]) {
        try {
            System.out.println("fechaRetorno 2:" + Utilidades.getStringDate("dd-MM-yyyy", "12-11-2018"));
        } catch (Exception ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getUrlBase(String uri, String path) {
        return uri.replaceFirst(path, "");
    }

}
